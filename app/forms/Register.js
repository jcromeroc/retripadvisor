import t from 'tcomb-form-native';
import formValidation from '../utils/Validation';

import inputTemplate from './templates/Input';

export const RegisterStruct = t.struct({
    name: t.String,
    email: formValidation.email,
    password: formValidation.password,
    passwordConfirmation: formValidation.password,
});

export const RegisterOptions = {
    fields: {
        name: {
            template: inputTemplate,
            config: {
                placeholder: "Nombre y apellido",
                iconType: "material-community",
                iconName: "account-outline",
            },
        },
        email: {
            template: inputTemplate,
            config: {
                placeholder: "Correo",
                iconType: "material-community",
                iconName: "at",
            },
        },
        password: {
            template: inputTemplate,
            config: {
                placeholder: "Contraseña",
                password: true,
                secureTextEntry: true,
                iconType: "material-community",
                iconName: "lock-outline",
            },
        },
        passwordConfirmation: {
            template: inputTemplate,
            config: {
                placeholder: "Repetir contraseña",
                password: true,
                secureTextEntry: true,
                iconType: "material-community",
                iconName: "lock-reset",
            },
        },
    }
};