import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';

import * as firebase from 'firebase';

export default class MyAccount extends React.Component {
    
    state = {
        login: false,
    };

    async componentDidMount() {
        await firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                console.log(user);
                this.setState({
                    login: true,
                });
            } else {
                this.setState({
                    login: false,
                }); 
            }
        });
    }

    onGoToScreen = (nameScreen) => {
        this.props.navigation.navigate(nameScreen);
    };

    logout = () => {
        firebase.auth().signOut();
    };

    render() {
        const { login } = this.state;
        
        if (login) {
            return (
                <View style={styles.viewBody}>
                    <Text>Estas logueado...</Text>
                    <Button title="Cerrar sesión" onPress={() => {this.logout()}}/>
                </View>
            );
        } else {
            return (
                <View style={styles.viewBody}>
                    <Text>MyAccount Screen...</Text>
                    <Button title="Registro" onPress={() => {this.onGoToScreen('Register')}}/>
                    <Button title="Login" onPress={() => {this.onGoToScreen('Login')}}/>
                </View>
            );
        }
    }
};

const styles = StyleSheet.create({
    viewBody: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#fff"
    }
});