import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';

import t from 'tcomb-form-native';
const Form = t.form.Form;

import { RegisterStruct, RegisterOptions } from '../../forms/Register';

import * as firebase from 'firebase';
import Toast, {DURATION} from 'react-native-easy-toast';

export default class Register extends React.Component {

    state = {
        registerStruct: RegisterStruct,
        registerOptions: RegisterOptions,
        formData: {
            name: '',
            email: '',
            password: '',
            passwordConfirmation: '',
        },
        formErrorMessage: '',
        errorColor: {
            color: '#f00',
        },
    };

    onRegister = () => {
        const  { password, passwordConfirmation } = this.state.formData;

        if (password === passwordConfirmation) {
            const validate = this.refs.registerForm.getValue();

            if (validate) {
                this.setState({
                    formErrorMessage: "",
                });
                firebase.auth().createUserWithEmailAndPassword(validate.email.trim(), validate.password)
                    .then((resolve) => {
                        console.log(resolve, 'Registro correcto');
                        this.setState({
                            formErrorMessage: 'Registro correcto',
                            errorColor: {
                                color: '#00a680',
                            },
                        });
                        this.refs.toast.show('Registro correcto', 200, () => {
                            this.props.navigation.navigate('MyAccount');
                        });
                    })
                    .catch((err) => {
                        console.log(err, 'El email ya esta en uso');
                        this.setState({
                            formErrorMessage: 'El email ya esta en uso',
                            errorColor: {
                                color: '#f00',
                            },
                        });
                        this.refs.toast.show('El email ya esta en uso', 200, () => {
                        });
                    });
            } else {
                this.setState({
                    formErrorMessage: "Formulario Incorrecto",
                    errorColor: {
                        color: '#f00',
                    },
                });
            }
        } else {
            this.setState({
                formErrorMessage: "Las contraseñas no son iguales",
                errorColor: {
                    color: '#f00',
                },
            });
        }
    };

    onChangeFormRegister = (formData) => {
        this.setState({
            formData
        });
    };

    render() {
        const { registerStruct, registerOptions, formData, formErrorMessage, errorColor } = this.state;
        return(
            <View style={styles.viewBody}>
                <Form 
                    ref="registerForm"
                    type={registerStruct}
                    options={registerOptions}
                    value={formData}
                    onChange={e => this.onChangeFormRegister(e)}
                />
                <Button
                    buttonStyle={styles.buttonRegisterContainer}
                    title="Unirse"
                    onPress={() => {this.onRegister()}}
                />
                <Text style={{...styles.formErrorMessage, ...errorColor}}>{formErrorMessage}</Text>
                <Toast
                    ref="toast"
                    position='bottom'
                    positionValue={250}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color:'#fff'}}
                />
            </View>
        );
    };
};

let styles = StyleSheet.create({
    viewBody: {
        flex: 1,
        justifyContent: "center",
        marginLeft: 40,
        marginRight: 40,
    },
    buttonRegisterContainer: {
        backgroundColor: "#00a680",
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
    },
    formErrorMessage: {
        textAlign: "center",
        marginTop: 30
    }
});