import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ActivityIndicator } from 'react-native';
import { Image, Button, SocialIcon, Divider } from 'react-native-elements';

import t from 'tcomb-form-native';
const Form = t.form.Form;

import * as firebase from 'firebase';
import * as Facebook from 'expo-facebook';
import Toast, {DURATION} from 'react-native-easy-toast';

import { LoginStruct, LoginOptions } from '../../forms/Login';

import FacebookApi from '../../utils/Social';

export default class Login extends React.Component {

    state = {
        loginStruct: LoginStruct,
        loginOptions: LoginOptions,
        loginData: {
            email: '',
            password: '',
        },
        formErrorMessage: '',
        errorColor: {
            color: '#f00',
        },
    };

    onLogin = () => {
        const validate = this.refs.loginForm.getValue();
        
        if (validate) {
            firebase.auth().signInWithEmailAndPassword(validate.email.trim(), validate.password)
            .then(() => {
                this.setState({
                    formErrorMessage: 'Login correcto',
                    errorColor: {
                        color: '#00a680',
                    },
                });
                this.refs.toastLogin.show('Login correcto', 200, () => {
                    this.props.navigation.goBack();
                });
                console.log('Login correcto firebase');
            })
            .catch((error) => {
                console.log('Login incorrecto firebase', error);
                this.setState({
                    formErrorMessage: "Login Incorrecto",
                    errorColor: {
                        color: '#f00',
                    },
                });
                this.refs.toastLogin.show('Login incorrecto', 200, () => {
                });
            });
        } else {
            this.setState({
                formErrorMessage: "Formulario Incorrecto",
                errorColor: {
                    color: '#f00',
                },
            });
        }
    };

    onLoginFacebook = async () => {
        const { type, token } = await Facebook.logInWithReadPermissionsAsync(
            FacebookApi.aplication_id,
            { permissions: FacebookApi.permissions }
        ); 

        console.log(type, token);
        if (type == 'success') {
            const credentials = firebase.auth.FacebookAuthProvider.credential(token);
            firebase.auth().signInWithCredential(credentials)
            .then(() => {
                this.setState({
                    formErrorMessage: 'Login correcto',
                    errorColor: {
                        color: '#00a680',
                    },
                });
                this.refs.toastLogin.show('Login correcto', 200, () => {
                    this.props.navigation.goBack();
                });
            })
            .catch((error) => {
                this.setState({
                    formErrorMessage: "Error al inciar con Facebook",
                    errorColor: {
                        color: '#f00',
                    },
                });
                this.refs.toastLogin.show('Error al inciar con Facebook', 200, () => {
                });
            });
        } else if (type == 'cancel') {
            this.setState({
                formErrorMessage: "Inicio de sesión cancelado",
                errorColor: {
                    color: '#f00',
                },
            });
            this.refs.toastLogin.show('Inicio de sesión cancelado', 200, () => {
            });
        } else {
            this.setState({
                formErrorMessage: "Error desconocido, intente luego",
                errorColor: {
                    color: '#f00',
                },
            });
            this.refs.toastLogin.show('Error desconocido, intente luego', 200, () => {
            });
        }
    };

    onChangeFormLogin = (loginData) => {
        this.setState({
            loginData,
        });
    };

    render() {
        const { loginStruct, loginOptions, loginData, formErrorMessage, errorColor } = this.state; 
        return (
            <View style={styles.viewBody}>
                <Image
                    source={require('../../../assets/img/5-tenedores-letras-icono-logo.png')}
                    containerStyle={styles.containerLogo}
                    style={styles.logo}
                    PlaceholderContent={<ActivityIndicator />}
                    resizeMode="contain"
                />
                <View style={styles.viewForm}>
                    <Form
                        ref="loginForm"
                        type={loginStruct}
                        options={loginOptions}
                        value={loginData}
                        onChange={e => this.onChangeFormLogin(e)}
                    />
                    <Button 
                        title="Login"
                        buttonStyle={styles.buttoLoginContainer}
                        onPress={() => this.onLogin()}
                    />
                    <Text style={{...styles.formErrorMessage, ...errorColor}}>{formErrorMessage}</Text>
                    <Divider
                        style={styles.divider}
                    />
                    <SocialIcon
                        title='Iniciar sesión con Facebook'
                        button
                        type='facebook'
                        onPress={() => {this.onLoginFacebook()}}
                    />
                </View>
                <Toast
                    ref="toastLogin"
                    position='bottom'
                    positionValue={250}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color:'#fff'}}
                />
            </View>
        );
    };
};

const styles = StyleSheet.create({
    viewBody: {
        flex: 1,
        marginLeft: 25,
        marginRight: 40,
        marginTop: 40,
    },
    containerLog: {
        alignItems: "center",
    },
    logo: {
       width: 300,
       height: 150, 
    },
    viewForm: {
        marginTop: 40,
    },
    buttoLoginContainer: {
        backgroundColor: "#00a680",
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
    },
    formErrorMessage: {
        textAlign: "center",
        marginTop: 20
    },
    divider: {
        backgroundColor: "#00a680",
    }
});